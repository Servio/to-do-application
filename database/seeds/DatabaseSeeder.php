<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Tipos;
use App\Status;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	
        DB::table('tipos')->insert([
		
          [  'namet' => 'Travels',],
          [  'namet' => 'Eletronics',],
	        [  'namet' => 'Music',],
          [  'namet' => 'Food',],


        ]);
        DB::table('statuses')->insert([
		
          [  'status' => 'new',],
          [  'status' => 'in progress',],
	        [  'status' => 'finished',],
          


        ]);

        DB::table('priorities')->insert([
		
          [  'namep' => 'hign',],
          [  'namep' => 'medium',],
	        [  'namep' => 'low',],
          


        ]);
    }
}
