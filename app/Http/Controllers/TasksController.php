<?php
use App\Tasks;
use App\Tipo;
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Status;
use App\User;
use App\priority;
use Log;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info("here");
        Log::info($request);
       

       $tasks = Tasks::all()->where('archive',true)
                            ->sortBy('priority');
       $statuses = Status::all();
        $types = Tipo::all();
        $priorities = priority::all();
        

        return view('tasks.index', compact('tasks','statuses','types','priorities'));
   
    }
    public function dropindex(Request $request)
    {
        
       
        return view('tasks.index', compact('tasks','statuses','type',));

    }

	public function storearchive(Request $request,$id)
    {
	Log::info($request);
	       
	$task = Tasks::find($id);
	//$task->save();
	$task = DB::table('tasks')
	->where('id', $id)
	->update(['archive'=> 0]);
        return redirect('/archive')->with('success', 'task updated!');

   
 }
 public function unarchive(Request $request,$id)
 {
 Log::info($request);
        
 $task = Tasks::find($id);
 //$task->save();
 $task = DB::table('tasks')
 ->where('id', $id)
 ->update(['archive'=> 1]);
     return redirect('/tasks')->with('success', 'task updated!');


}
    
	public function archive()
    {
       $tasks = Tasks::all();
         $type = Tipo::all();
        $statuses = Status::all();
        $ciao = Status::all();
        return view('/archive',compact('tasks','ciao'));
   
 }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	

    public function create()
    {
	
    $type = Tipo::all();
    $statuses = Status::all();
    $priorities = priority::all();
        return view('tasks.create',compact('type','statuses','priorities'));
	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	Log::info($request);
        $request->validate([
            'nametask'=>'required',
            'descri'=>'required',
        'type_id' =>'required',
        'status' =>'required',
        'completiondate' =>'required',
        'priority'  => 'required'
        ]);

        $task = new Tasks([
            'nametask' => $request->get('nametask'),
            'descri' => $request->get('descri'),
            'completiondate' => $request->get('completiondate'),
            
        ]);

    $task->tipo_id = $request->get('type_id');
    $task->status = $request->get('status');
    $task->priority = $request->get('priority');
	Log::info($task);
	$task->archive =(1);
        $task->save();
        return redirect('/tasks')->with('success', 'Task saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $task = Tasks::find($id);
        $type = Tipo::all();
        $statuses = Status::all();
        $priorities = priority::all();
        return view('tasks.show',compact('task','type','statuses','priorities'));
     }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    	{
        $task = Tasks::find($id);
        $type = Tipo::all();
        $statuses = Status::all();
        $priorities = priority::all();
        return view('tasks.edit',compact('task','type','statuses','priorities'));        
   
   	 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nametask'=>'required',
            'descri'=>'required',
            'type_id' =>'required',
            'status' =>'required',
            'completiondate' =>'required',
            'priority'  => 'required'
        ]);

        $task = Tasks::find($id);
        $task->nametask =  $request->get('nametask');
        $task->descri = $request->get('descri');
        $task->tipo_id = $request->get('type_id');
        $task->status = $request->get('status');
        $task->completiondate = $request->get('completiondate');
        $task->priority = $request->get('priority');
        
        $task->save();

        return redirect('/tasks')->with('success', 'task updated!');
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $task = Tasks::find($id);
        $task->delete();

        return redirect('/tasks')->with('success', 'Task deleted!');
   
    }
	public function destroya($id)
    {
       $task = Tasks::find($id);
        $task->delete();

        return redirect('/archive')->with('success', 'Task deleted!');
   
    }

}
