<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
     protected $fillable = [
        'nametask',
        'descri',
	'date',
        'archive',
        'status',
        'completiondate',
        'priority'
];
}
