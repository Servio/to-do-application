<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
            'id',
           'status',
        ];
        }
