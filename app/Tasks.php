<?php

namespace App;
use App\Tipo;
use App\Status;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
     protected $fillable = [
        'nametask',
        'descri',
	    'date',
        'archive',
        'status',
        'completiondate'
];
}
public function tipo()
    {
        return $this->hasOne(Tipo.class);
    }
public function status()
    {
        return $this->hasOne(Status.class);
    }
}

