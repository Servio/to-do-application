@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Are u sure to delete a task??</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('tasks.destroy'), $task->id }}">
          @csrf
	@method
         
          <button type="submit" class="btn btn-primary-outline">Delete task</button>
      </form>
  </div>
</div>
</div>
@endsection
