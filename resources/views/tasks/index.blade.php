@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Tasks</h1>    
  <table class="table table-striped">
    <thead>
	@if(Auth::check())
		<p>You are logged</p>
        <tr>
			<td>Priority</td>
            <td>ID</td>
            <td>Nametask</td>
            <td>Description</td>
	  		<td>Type</td>	
			<td>Status</td>
		    <td>Completion Date</td>
		
          <td colspan = 4 >Actions</td>
        </tr>
    </thead>
    <tbody>
	<div>
   	 	<a style="margin: 19px;" href="{{ route('tasks.create')}}" 				class="btn btn-primary">New Task</a>
  
   	 	<a style="margin: 25px;" href="{{ route('archive')}}" class="btn btn-primary">Archive</a>
			<!--			@php 
						$status = $statuses;
						@endphp
			<form href="{{ route('tasks.index')}}" method="get">
                   				
					<button class="btn btn-danger" action="submit" value="{{$status[1]->id}}">new</button>
					<input type="text" value="2" />
					
				      <select name="status">                      
                            <option value="null">All</option>
                            @foreach ($statuses as $status)
                                <option value="{{ $status->id }}" >{{$status->status}}</option>
                            @endforeach
                        </select>
						{{$status->id}}
						
            </form>-->

			
			<!--<div class="form-group">
  		<select class="btn btn-white" style="margin: 25px;" name="status"	href="{{ route('tasks.index')}}">
		  <option> </option>
		  @foreach($statuses as $status)
		  
		  <option value="{{$status->id}}"  > 
		  {{$status->status}}
			
		  
		  </option>
		  @endforeach
		  </select>
		{{$tasks}}
		{{$status->id}}
		
		</div>-->
		

	
		
    </div>  
  	
		</div>

	<div class="col-sm-12">
	
  		@if(session()->get('success'))
   	 		<div class="alert alert-success">
      				{{ session()->get('success') }}  
    			</div>
  		@endif
		 
        @foreach($tasks as $task)
			@php
				$date = date('Y-m-d');
				
				$color =['white','#5cd65c'];
				if($task->completiondate < $date)
						$hi = 1;
					else	
					 	$hi = 0;
						
			@endphp
					     
									<tr style='background-color:{{$color[$hi]}}'>
									<td>
									@foreach($priorities as $priority)
											@if($priority->id === $task->priority)	
												 {{$priority->namep}}
											@endif
										@endforeach
									</td>
									<td>{{$task->id}}</td>
									<td>{{$task->nametask}}</td>
									<td>{{$task->descri}}</td>
									<td>
										@foreach($types as $type)
											@if($type->id === $task->tipo_id)	
												 {{$type->namet}}
											@endif
										@endforeach
				
									</td>
									<td>
										@foreach($statuses as $status)
											@if($status->id === $task->status)	
												 {{$status->status}}
											@endif
										@endforeach
									
									</td>
									<td>{{$task->completiondate}}</td>

									<td>
										<a href="{{ route('tasks.show',$task->id)}}" class="btn btn-primary">Show</a>
									</td>

									<td>
										<a href="{{ route('tasks.edit',$task->id)}}" class="btn btn-primary">Edit</a>
									</td>
									<td>
										<form action="{{ route('tasks.destroy', $task->id)}}" method="post">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger" action="submit">Delete
												</button>
										
										</form>
									<td>
										<a href="{{ route('storearchive', $task->id)}}" class="btn btn-primary"	value="$task->id">Store in Archive</a>
										</td>            
									
				
			
								</tr>
						
        @endforeach
		@endif
		@if(Auth::guest())
              <a href="/login" class="btn btn-info"> You need to login to see the Tasks's list >></a>
            @endif
	</tbody>
  </table>
<div>
</div>
@endsection
