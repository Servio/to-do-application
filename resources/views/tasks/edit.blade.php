@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a task</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('tasks.update', $task->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">
                <label for="priority">Priority:</label>
                <select class="form-control" name="priority">
                <option> @foreach($priorities as $priority)
											@if($priority->id === $task->priority)	
												 {{$priority->namep}}
											@endif
                                        @endforeach
                                    </option>
                    @foreach($priorities as $priority)
                    
                    <option value="{{$priority->id}}"  > 
                    {{$priority->namep}}
                        
                    
                    </option>
                    @endforeach
                </option>
                </select>
                </div> 

            <div class="form-group">

                <label for="nametask">nametask:</label>
                <input type="text" class="form-control" name="nametask" value={{ $task->nametask }} />
            </div>

            <div class="form-group">
                <label for="descri">descri:</label>
                <input type="text" class="form-control" name="descri" value={{ $task->descri }} />
            </div>
			<label for="type_id">Type:</label>
  		<select class="form-control" name="type_id">
			
		<option value="{{$task->tipo_id}}">
                                      @foreach($type as $types)
											@if($types->id === $task->tipo_id)	
												 {{$types->namet}}
											@endif
                                        @endforeach
				
		 </option>
    		@foreach ($type as $tipo)
		<option value="{{$tipo->id}}">
		{{$tipo->namet}}
		@endforeach	
		</option>
          </select>
          
          <div class="form-group">
  	<label for="status">Status:</label>
  	<select class="form-control" name="status">
	<option>                            @foreach($statuses as $status)
											@if($status->id === $task->status)	
												 {{$status->status}}
											@endif
                                        @endforeach
                                    </option>
            @foreach($statuses as $status)
		  
		  <option value="{{$status->id}}"  > 
		  {{$status->status}}
			
		  
		  </option>
		    @endforeach
	</option>
  	</select>
     </div> 
                    <div class="form-gruop">
                    <label for="completiondate">Completion Date:</label>
                    <input type="date" id="completiondate" name="completiondate"
                            value="{{ $task->completiondate }} ">
                    </div>
		<br></br>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
