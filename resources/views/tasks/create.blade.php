@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a task</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('tasks.store') }}">
          @csrf
          <div class="form-group">    
              <label for="nametask">Name Task:</label>
              <input type="text" class="form-control" name="nametask"/>
          </div>

          <div class="form-group">
              <label for="descri">Description:</label>
              <input type="text" class="form-control" name="descri"/>
          </div>
	
	 <div class="form-group">
  	<label for="type_id">Type:</label>
  	<select class="form-control" name="type_id">
	<option> </option>
    	@foreach ($type as $tipo)
	<option value="{{$tipo->id}}">
	{{$tipo->namet}}
	@endforeach	
	</option>
  	</select>
	</div> 
  <div class="form-group">
  	<label for="status">Status:</label>
  	<select class="form-control" name="status">
	<option> </option>
  @foreach($statuses as $status)
		  
		  <option value="{{$status->id}}"  > 
		  {{$status->status}}
			
		  
		  </option>
		  @endforeach
	</option>
  	</select>
  </div> 
  <div class="form-group">
  	<label for="priority">Priority:</label>
  	<select class="form-control" name="priority">
	<option> </option>
  @foreach($priorities as $priority)
		  
		  <option value="{{$priority->id}}"  > 
		  {{$priority->namep}}
			
		  
		  </option>
		  @endforeach
	</option>
  	</select>
	</div> 
    <div class="form-gruop">
    <label for="completiondate">Completion Date:</label>
    <input type="date" id="completiondate" name="completiondate"
            value="@php
            $date = new DateTime();
              echo $date->getTimestamp();
            @endphp" >
    </div>
          <button type="Add" class="btn btn-primary-outline">Add task</button>
      </form>
  </div>
</div>
</div>
@endsection
