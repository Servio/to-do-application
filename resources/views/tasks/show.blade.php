@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">View a Tasks</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form  action="{{ route('tasks.index')}}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

            <label for="nametask">Priority :  </label>
            <label for="nametask">
                                        @foreach($priorities as $priority)
											@if($priority->id === $task->priority)	
												 {{$priority->namep}}
											@endif
										@endforeach
            </label>

            </div>

            <div class="form-group">

                <label for="nametask">Name of task:  </label>
                <label for="nametask">{{ $task->nametask }}</label>
               
            </div>

            <div class="form-group">
                <label for="descri">Description :  </label>
                <label for="descri">{{ $task->descri }}</label>
                
            </div>

            <div class="form-group">
                <label for="tipo_id">Type :     </label>
                <label for="tipo_id">
                                        @foreach($type as $types)
											@if($types->id === $task->tipo_id)	
												 {{$types->namet}}
											@endif
                                        @endforeach
                </label>
                
            </div>
            <div class="form-group">
                <label for="status">Status :  </label>
                <label for="status">       
                                    @foreach($statuses as $status)
											@if($status->id === $task->status)	
												 {{$status->status}}
											@endif
										@endforeach
                           
                </label>
                
             </div>
             <div class="form-group">
                <label for="completiondate">Completion Date :  </label>
                <label for="completiondate">{{ $task->completiondate }}</label>

                    
		            <br></br>
            <button type="submit" class="btn btn-primary">Go back</button>
        </form>
    </div>
</div>
@endsection

