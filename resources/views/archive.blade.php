@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Task's Archive</h1>
<table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nametask</td>
          <td>Description</td>
	  <td>Type</td>
          <td colspan = 3>Actions</td>
        </tr>
    </thead>
	<div>
   	 	<a style="margin: 50px;" href="{{ route('tasks.index')}}" class="btn btn-primary">Tasks</a>
    	</div> 
 
	 @foreach($tasks as $task)
   @if($task->archive === false)
        <tr> 
            <td>{{$task->id}}</td>
            <td>{{$task->nametask}}</td>
	    <td>{{$task->descri}}</td>
	    <td>{{$task->archive}}</td>

	    <td>
		@php
		$tipo =	$task->tipo_id;
		
		$tipos = DB::table('tipos')
		->where('id',$tipo)	
		->first();
			
		echo $tipos->namet;
		@endphp
		
	    </td>
		<td>
                <a href="{{ route('tasks.edit',$task->id)}}" class="btn btn-primary">Edit</a>
            </td>
		<td>
               <form action="{{ route('task.destroy', $task->id)}}" method="post">
                  @csrf
		  @method('DELETE')
                  <button class="btn btn-danger" action="submit">Delete</button>
            <td>
	<a href="{{ route('unarchive', $task->id)}}" class="btn btn-primary"	value="$task->id">Remove from Archive</a>
            </td> 
                </form>
            </td>
	<br />
	@endif
     	</tr>
  	</div>
	@endforeach
</div>
</div>
@endsection
