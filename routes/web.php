<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('tasks', 'TasksController');
Route::any('/archive','TasksController@archive')->name('archive');
Route::any('/archive/destroy/{id}','TasksController@destroya')->name('task.destroy');
Route::any('/archive/store/{id}','TasksController@storearchive')->name('storearchive');
Route::any('/archive/unstore/{id}','TasksController@unarchive')->name('unarchive');
Route::auth();
Route::get('/got', [
    'middleware' => ['auth'],
    'uses' => function () {
     echo "You are allowed to view this page!";
  }]);

;

Route::get('/', function () {
    return view('/tasks');
});





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
